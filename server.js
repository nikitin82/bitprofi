window.app || (app = angular.module('personApp'));

app.constant('bpHttpStatus', {Success: 200, ServerError: 500});

app.factory('personStorage', function() {
	if (window.openDatabaseSync) {
		var exec = function(query, params) {
			var result, db = openDatabaseSync('BitProfi', '1.0', 'Persons', null);
			db && db.transaction(function(t) {
				t.executeSql('CREATE TABLE IF NOT EXISTS persons(' +
					'id INTEGER PRIMARY KEY AUTOINCREMENT,' +
					'firstName VARCHAR(255) NOT NULL,' +
					'lastName VARCHAR(255) NOT NULL,' +
					'comment TEXT' +
					')', [], function () {
					db.transaction(function(t) {
						t.executeSql(query, params, function (t, res) {
							result = res;
						});
					});
				});
			});
			return result;
		};

		return {
			select: function(opt_id) {
				var query = 'SELECT * FROM persons';
				var params = [];
				if (opt_id) {
					query += ' WHERE id=?';
					params.push(opt_id);
				}
				if (query = exec(query, params)) {
					query = query.rows;
					return opt_id ? (query.length ? query[0] : null) : query;
				}
				return null;
			},
			insert: function(person) {
				var res = exec(
					'INSERT INTO persons(firstName,lastName,comment) VALUES(?,?,?)',
					[person.firstName, person.lastName, person.comment]
				);
				return res ? res.insertId : null;
			},
			update: function(person) {
				return !!exec(
					'UPDATE persons SET firstName=?,lastName=?,comment=? WHERE id=?',
					[person.firstName, person.lastName, person.comment, person.id]
				);
			},
			delete: function(id) {
				return !!exec('DELETE FROM persons WHERE id=?', [id]);
			}
		};
	}

	return (function($) {
		if (window.localStorage) {
			var storage = {
				get: function(key) {
					var value = localStorage['person.' + key];
					return value == null ? value : $.fromJson(value);
				},
				set: function(key, value) {
					localStorage['person.' + key] = $.toJson(value);
				},
				empty: function(opt_key) {
					opt_key ? localStorage.removeItem('person.' + opt_key) : localStorage.clear();
				}
			};

			return {
				select: function(opt_id) {
					if (opt_id) {
						return storage.get(opt_id);
					}
					var rows = [];
					for (var i = 1, ai = storage.get(0); i <= ai; ++i) {
						var person = storage.get(i);
						person && rows.push(person);
					}
					return rows;
				},
				insert: function(person) {
					var ai = (storage.get(0) || 0) + 1;
					storage.set(ai, $.extend(person, {id: ai}));
					storage.set(0, ai);
					return ai;
				},
				update: function(person) {
					storage.set(person.id, person);
					return true;
				},
				delete: function(id) {
					storage.empty(id);
					return true;
				}
			};
		}

		return {select: $.noop, insert: $.noop, update: $.noop, delete: $.noop};
	})(angular);
});

app.run(['$httpBackend', 'personURI', 'bpHttpStatus', 'personStorage', function(backend, URI, httpStatus, storage) {
	backend.whenGET(URI).respond(function(method, url, opt_id) {
		var rows = storage.select(opt_id);
		return rows ? [httpStatus.Success, rows] : [httpStatus.ServerError];
	});
	backend.whenPOST(URI).respond(function(method, url, person) {
		var id = storage.insert(angular.fromJson(person));
		return id ? [httpStatus.Success, id] : [httpStatus.ServerError];
	});
	backend.whenPUT(URI).respond(function(method, url, person) {
		return [storage.update(angular.fromJson(person)) ? httpStatus.Success : httpStatus.ServerError];
	});
	backend.whenDELETE(URI).respond(function(method, url, id) {
		return [storage.delete(id) ? httpStatus.Success : httpStatus.ServerError];
	});
}]);

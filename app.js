/** @return {string} */
String.prototype.lcFirst = function() {
	return this.charAt(0).toLowerCase() + this.substr(1);
};

/** @return {string} */
String.prototype.ucFirst = function() {
	return this.charAt(0).toUpperCase() + this.substr(1);
};

/** @typedef {{id: number, firstName: string, lastName: string, comment: string}} Person */

app = angular.module('personApp', ['ngMockE2E'], ['$provide', '$httpProvider', function(provide, httpProvider) {
	provide.factory('bpHttpError', ['$q', function($q) {
		return {
			responseError: function(response) {
				alert('Internal Server Error');
				return $q.reject(response);
			}
		};
	}]);
	httpProvider.interceptors.push('bpHttpError');
}]);

app.constant('personURI', '/person');

app.controller('personCtrl', ['$scope', '$http', 'personURI', function(scope, http, URI) {
	var $editor = $('#personEditor').on('shown.bs.modal', function() {
		$(this).find('input:first').focus();
	});

	var modalHide = function() {
		$editor.modal('hide');
	};

	var update = function() {
		scope.persons[scope.person.id] = scope.person;
	};

	$.extend(scope, {
		edit: function(opt_person) {
			scope.person = angular.copy(opt_person);
			$editor.modal();
		},
		remove: function(person) {
			confirm('Do you want to remove "' + person.firstName + ' ' + person.lastName + '"?')
				&& http.delete(URI, {data: person.id}).success(function() {
					delete scope.persons[person.id];
				});
		},
		submit: function() {
			var promise = scope.person.id
				? http.put(URI, scope.person).success(update)
				: http.post(URI, scope.person).success(function(id) {
					scope.person.id = id;
					update();
				});
			promise.success(modalHide);
		}
	});

	$editor.on('keydown', 'input', function(e) {
		if (e.keyCode == 13/*enter*/) {
			scope.personForm.$valid && scope.submit();
			return false;
		}
	});

	http.get(URI).success(function(persons) {
		scope.persons = {};
		for (var i in persons) {
			scope.person = persons[i];
			update();
		}
	});
}]);

app.directive('bpFormField', ['$compile', function(compile) {
	var attrNotEmpty = function(value) {
		return !((value == null) || (value == '0') || (value.toLowerCase() == 'false'));
	};

	return {
		link: function(scope, $el, attrs) {
			var $label = $el.find('label');
			var $input = $el.find('input,textarea').first();
			attrs.$observe('bpModel', function(value) {
				if (!value) {
					return;
				}
				var chunks = value.split('.');
				var id = chunks.shift().lcFirst();
				while (chunks.length) {
					id += chunks.shift().ucFirst();
				}
				$label.attr('for', id);
				compile($input.attr({name: value.replace(/\.([^.]+)/g, '[$1]'), id: id, 'ng-model': value}))(scope);
			});
			attrs.$observe('required', function(value) {
				$input.attr('required', value);
				$label.toggleClass('bp-required', attrNotEmpty(value));
			});
		},
		replace: true,
		restrict: 'E',
		template: function($el, attrs) {
			var tag, o = {'class': 'form-control'};
			attrs.type || (attrs.type = 'text');
			switch (attrs.type.toLowerCase()) {
				case 'textarea':
					tag = 'textarea';
					o['aria-multiline'] = true;
					break;
				default:
					tag = 'input';
					o.type = attrs.type;
			}
			attrNotEmpty(attrs.bpNamed) && $.extend(o, {
				maxlength: 255,
				pattern: '^[A-Za-zА-Яа-яЁё]+(-[A-Za-zА-Яа-яЁё]+)*$',
				required: true
			});
			return '<div class="form-group">' +
				'<label ng-transclude></label>' +
				$(['<', '>'].join(tag), o)[0].outerHTML +
			'</div>';
		},
		transclude: true
	};
}]);
